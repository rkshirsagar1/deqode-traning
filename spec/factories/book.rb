FactoryBot.define do
  factory :book do
    user
    title { 'book name' }
    release_date { '1999-12-10' }
  end
end