FactoryBot.define do
  factory :user do
    email { 'abc@example.com' }
    user_name { 'user' }
    password { '12345678' }
  end
end