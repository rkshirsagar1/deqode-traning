
require 'rails_helper'

module Mutations
  module Books
    RSpec.describe UpdateBook, type: :request do

      def query(id:)
        <<~GQL
          mutation {
            updateBook(
              id: #{id}
              title: "Tripwire"
              releaseDate: "1999-12-10"
            ) {
              id
              title
              releaseDate
              user {
                id
              }
            }
          }
        GQL
      end

      describe '#resolve' do

        let(:user) { create(:user) }
        let(:book) { create(:book, title: 'Hero', release_date: '1999-12-10', user: user) }

        it 'updates a book' do
          post '/graphql', params: { query: query(id: book.id) }
          expect(book.reload).to have_attributes(
            'user_id'      => user.id,
            'title'        => 'Tripwire'
          )
        end

        it 'returns a book' do
          post '/graphql', params: { query: query(id: book.id) }
          json = JSON.parse(response.body)
          data = json['data']['updateBook']
          expect(data).to include(
            'id'              => book.id.to_s,
            'title'           => 'Tripwire',
            'releaseDate' => "1999-12-10",
            'user'          => { 'id' => user.id.to_s }
          )
        end
      end

    end
  end
end