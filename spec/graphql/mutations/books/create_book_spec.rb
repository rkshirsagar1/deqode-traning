
require 'rails_helper'

module Mutations
  module Books
    RSpec.describe CreateBook, type: :request do

      def query(user_id:)
        <<~GQL
        mutation {
          createBook(
            userId: #{user_id},
            title: "book name",
            releaseDate: "1999-12-10"
          ) {
            id
            title
            releaseDate
            user {
              id
              email
            }
          }
          }
        GQL
      end

      describe '#resolve' do

        let(:user) { create(:user) }

        it 'creates a book' do
          expect do
            post '/graphql', params: { query: query(user_id: user.id) }
          end.to change { Book.count }.by(1)
        end

        it 'returns a book' do
          post '/graphql', params: { query: query(user_id: user.id) }
          json = JSON.parse(response.body)
          data = json['data']['createBook']
          expect(data).to include(
            'id'              => be_present,
            'title'           => "book name",
            'releaseDate'     => "1999-12-10",
            'user'          => { "email" => "abc@example.com", "id" => user.id.to_s }
          )
        end
      end
    end
  end
end
