require 'rails_helper'

module Mutations
  module Books
    RSpec.describe DestroyBook, type: :request do
      
      def query(id:)
        <<~GQL
          mutation {
            destroyBook(
              id: #{id}
            ) {
              id
              title
              user {
                id
              }
            }
          }
        GQL
      end
      
      describe '#resolve' do

        let(:user) { create(:user) }
        let(:book) { create(:book, title: 'Hero', release_date: '1999-12-10', user: user) }

        it 'removes a book' do
          expect do
            post '/graphql', params: { query: query(id: book.id) }
          end.to change { Book.count }.by(0)
        end

        it 'returns a book' do
          post '/graphql', params: { query: query(id: book.id) }
          json = JSON.parse(response.body)
          data = json['data']['destroyBook']
          expect(data).to include(
            'id'              => be_present,
            'title'           => 'Hero',
            'user'          => { 'id' => be_present }
          )
        end
      end
    end
  end
end




