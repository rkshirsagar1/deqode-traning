
require 'rails_helper'

describe 'POST /graphql #get_book', type: :request do

  let(:user) { create(:user) }
  let(:book) { create(:book, user: user) }

  let(:result_data) do
    {
      "title"=>"book name"
    }
  end

  def query
    <<~GQL
    {
      getBook(id: #{book.id}) {
        title
      }
    }
    GQL
  end

  describe '#resolve' do
    it 'gets specific book by id' do
      post '/graphql', params: { query: query }
      json = JSON.parse(response.body)
      data = json['data']['getBook']
      expect(data).to(match(result_data))
    end
  end
end
