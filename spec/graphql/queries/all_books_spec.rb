
require 'rails_helper'

describe 'POST /graphql #all_books', type: :request do

  let(:user) { create(:user) }
  let(:book) { create(:book, user: user) }

  let(:result_data) do
    [
      {
        "id"=>"7",
        "title"=>"book name",
        "releaseDate"=>"1999-12-10", 
        "user"=>{"email"=>"abc@example.com"}
      }
    ]
  end

  def query
    <<~GQL
    {
      allBooks {
        title
        releaseDate
        id
        user {
          email
        }
      }
    }
    GQL
  end

  describe '#resolve' do
    it 'displays all books' do
      book
      post '/graphql', params: { query: query }
      json = JSON.parse(response.body)
      data = json['data']['allBooks']
      expect(data).to(match(result_data))
    end
  end
end
