class PublishArticleJob < ApplicationJob
  
  def perform
    articles = Article.where(published: false)
    articles.each do |article|
      article.update(published: true) if article.created_at < (Time.now - 24.hours) 
    end
  end
end