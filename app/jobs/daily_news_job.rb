class DailyNewsJob < ApplicationJob
  
  def perform
    NewsHeadline.all.delete_all
    news = News.new('https://zeenews.india.com/rss/india-national-news.xml')
    news.extract.each do |headline, link| 
      NewsHeadline.create!(headline: headline, link: link)
    end
  end
end