# frozen_string_literal: true

# require 'nokogiri'
# require 'open-uri'
class News
  # @return [String] rss_feed URL of RSS news feed
  attr_reader :rss_feed

  # New news feed
  #
  # @param [String] rss_feed
  def initialize(rss_feed)
    @rss_feed = rss_feed
  end

  # Extracts news headlines, links from rss feed
  #
  # @return [Array<Array>] Each nested array contains news headline, link
  def extract
    daily_news = Nokogiri::XML(URI.parse(@rss_feed).open)
    news_headlines = daily_news.search('item').search('title').map(&:text)
    news_links = daily_news.search('item').search('link').map(&:text)
    news_headlines.zip(news_links)
  end
end

# news = News.new('https://zeenews.india.com/rss/india-national-news.xml')
# puts news.extract_headlines
