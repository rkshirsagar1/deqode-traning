# frozen_string_literal: true

class DetailedExecutionError < GraphQL::ExecutionError
  attr_accessor :type, :fields

  def initialize(message, ast_node: nil, type: nil, fields: nil)
    self.type = type
    self.fields = fields
    super(message, ast_node: ast_node)
  end

  def to_h
    if type
      res = {
        type: type,
        message: message,
      }
      if %i[validation authentication].include?(type)
        res[:fields] = fields
      else
        res[:details] = fields
      end
      res
    else
      super
    end
  end
end
