# require 'pathway'
  module Operations
    module Book
      class Create < Pathway::Operation

        context :current_user

        contract do
          option :current_user
      
          params do
            required(:title).value(:string)
            required(:release_date).value(:date)
          end   

          rule(:title) do
            key.failure(:invalid_title) if value.length < 3
          end

          rule(:release_date) do
            key.failure(:invalid_release_date) if value >= Date.today
          end

        end

        process do
          step :validate 
          set :create_book
        end

        def create_book(params, **)
          current_user.books.create(params[:input])
        end

      end
    end
  end


