module OperationHelper
  def convert_operation_error(error)
    fields = error.details.transform_keys.each{ |key| key.to_s.camelize(:lower).to_sym }
    DetailedExecutionError.new(error.message, ast_node: nil, type: error.type, fields: fields)
  end
end