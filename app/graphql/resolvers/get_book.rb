module Resolvers
  class GetBook < GraphQL::Schema::Resolver

    argument :id, Int, required: true
    type Types::BookType, null: false

    def resolve(id:)
      Book.find(id)
    rescue ActiveRecord::RecordNotFound 
      GraphQL::ExecutionError.new("Record not found")
    end
  end
end