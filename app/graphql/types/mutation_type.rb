module Types
  class MutationType < Types::BaseObject
    field :create_Book, mutation: Mutations::Books::CreateBook
    field :update_Book, mutation: Mutations::Books::UpdateBook
    field :destroy_Book, mutation: Mutations::Books::DestroyBook
    field :create_user, mutation: Mutations::Users::CreateUser
  end
end
