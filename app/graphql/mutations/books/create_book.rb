module Mutations
  module Books
    class CreateBook < Mutations::BaseMutation
      # arguments passed to the `resolve` method
      argument :title, String, required: true
      argument :release_date, String, required: true
      argument :user_id, Integer, required: true

      # return type from the mutation
      payload_type Types::BookType

      def resolve(user_id:, **params)
        book = Operations::Book::Create.new(current_user: User.find(user_id))
        res = book.call(params)
        if res.success?
          res.value
        else
          convert_operation_error(res.error)
        end
      end
    end
  end
end