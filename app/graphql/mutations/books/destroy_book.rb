module Mutations
  module Books
    class DestroyBook < Mutations::BaseMutation
      # arguments passed to the `resolve` method
      argument :id, Integer, required: true

      # return type from the mutation
      type Types::BookType

      def resolve(id:)
        Book.find(id).destroy
      rescue ActiveRecord::RecordNotFound 
        GraphQL::ExecutionError.new("Record not found")
      end
    end
  end
end