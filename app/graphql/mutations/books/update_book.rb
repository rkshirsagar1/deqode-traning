module Mutations
  module Books
    class UpdateBook < Mutations::BaseMutation
      # arguments passed to the `resolve` method
      argument :id, Integer, required: true
      argument :title, String, required: false
      argument :release_date, String, required: false
      argument :user_id, Integer, required: false

      # return type from the mutation
      type Types::BookType

      def resolve(id:, **attributes)
        Book.find(id).tap do |book|
          book.update!(attributes)
        end
      rescue => e
        GraphQL::ExecutionError.new("#{e.message}")
      end
    end
  end
end