module Mutations
  module Users
    class CreateUser < Mutations::BaseMutation
      # arguments passed to the `resolve` method
      argument :email, String, required: true
      argument :password, String, required: true
      argument :user_name, String, required: false
      argument :password_confirmation, String, required: true
      
      type Types::UserType
  
      def resolve(**attributes)
        User.create!(attributes)
      rescue ActiveRecord::RecordInvalid => e
        GraphQL::ExecutionError.new("#{e.record.errors.full_messages.join(', ')}")
      end
    end
  end
end