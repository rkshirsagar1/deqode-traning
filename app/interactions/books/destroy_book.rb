class DestroyBook < ActiveInteraction::Base
  object :book

  def execute
    book.destroy
  end
end