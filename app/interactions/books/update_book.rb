class UpdateBook < ActiveInteraction::Base

  object :book

  string :title
  date :release_date

  def execute
    book.update(title: title, release_date: release_date)
    errors.merge!(book.errors) unless book.save
    book
  end

end
