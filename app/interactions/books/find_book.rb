class FindBook < ActiveInteraction::Base
  integer :id

  def execute
    book = Book.find_by_id(id)
    return book if book
    errors.add(:id, 'does not exist')
  end
end