class ListBooks < ActiveInteraction::Base
  def execute
    Book.order(title: :asc)
  end
end