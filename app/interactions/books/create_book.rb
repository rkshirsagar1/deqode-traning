class CreateBook < ActiveInteraction::Base
  
  # Filter inputs
  string :title
  date :release_date
  string :user_id

  def to_model
    Book.new
  end

  def execute
    book = Book.new(inputs)
    errors.merge!(book.errors) unless book.save
    book
  end
end