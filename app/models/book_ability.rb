# frozen_string_literal: true

class BookAbility
  include CanCan::Ability

  def initialize(user)
    can :manage, Book, user_id: user.id
  end
end