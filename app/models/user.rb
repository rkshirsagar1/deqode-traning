class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  
  after_create :send_welcome_mail
  has_many :books, dependent: :destroy

  def send_welcome_mail
    UserMailer.with(user: self).welcome_email.deliver_later
  end
end
