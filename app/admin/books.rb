ActiveAdmin.register Book do
  actions :all, except: [:destroy, :new]
  permit_params :title, :release_date, category_ids:[]

  index do
    selectable_column
    id_column
    column :title
    column :release_date
    column :categories
    actions
  end

  show do
    attributes_table do
      row :title
      row :release_date
      row :user
      row :categories
    end
  end

  form do |f|
    f.inputs do
      collected_data = Category.all.map{|category| [category.name, category.id]}
      f.input :category_ids, label: 'Category', as: :check_boxes, collection: collected_data
    end
    f.actions
  end

  filter :title
  filter :release_date
  filter :categories
end