
ActiveAdmin.register Article do
  # Provides an option to filter records on index page
  scope("Article 1") { |scope| scope.where(name: 'Article 1') }
  scope :all, default: true
  config.create_another = true
  # Overwriting index view for articles
  index do

    selectable_column
    id_column
    # We can define name of a column to be displayed
    column 'Title' do |article|
      link_to article.name, admin_article_path(article)
    end
    column :description
    column :published
    actions defaults: true do |article|
      link_to('Timestamps', timestamps_admin_article_path(article))
    end
  end

  # Display show block
  # show do
  #   h1 :Name 
  #   h3 resource.name
  #   h1 :Description
  #   div do
  #     simple_format resource.description
  #   end
  # end

  show do
    attributes_table do
      row :name
      row :description
    end
    active_admin_comments
  end

  member_action :timestamps, method: :get do

  end

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :name, :description
  #
  # or
  #
  # permit_params do
  #   permitted = [:name, :description]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end