class BooksController < ApplicationController
  
  before_action :authenticate_user!
  load_and_authorize_resource 

  # Render list of all {Book}
  def index
    # @books = current_user.books

    # Includes gives all of the books doesnt care if it is assiciated with user or not
    # Uses each resource seprately (2 queries)
    # Includes - include all (preloads in memeory)
    # Can be used to avoid N + 1 problem
    # acts like preload by default
    # Acts like eager_load if you reference the associated tables
    @books = Book.includes(:user).where(user_id: current_user.id)

    # Only give books which have user associated with it (singe query - inner join)
    # Joins - only with matching books and users
    # But it causes N + 1 query problem for association
    # @books = Book.joins(:user).where(user_id: current_user.id)

    # Single query (left outer joins)
    # can reference the other table columns in where condition
    # @books = Book.eager_load(:user).where(user_id: current_user.id)
    # @books = ListBooks.run!
  end

  # Renders a form to create new {Book}
  def new
    @book = CreateBook.new
  end

  # Creates new {Book}
  def create
    @book = CreateBook.run(book_params)
    return redirect_to @book.result, notice: t('books.create.book') if @book.valid?
    render 'new'
  end

  # Renders a form to edit {Book}
  def edit
    @book = find_book!
  end

  # Update {Book}
  def update
    inputs = { book: find_book! }.reverse_merge(params[:book])
    outcome = UpdateBook.run(inputs)
    if outcome.valid?
      redirect_to outcome.result, notice: t('books.update.book')
    else
      @book = outcome
      render(:edit)
    end
  end

  # Displays specific {Book} details
  def show
    @book = find_book!
  end

  # Deletes a {Book}
  def destroy
    DestroyBook.run!(book: find_book!)
    redirect_to books_url, notice: t('books.destroy.book')
  end

  private

  # Finds a {Book} by ID
  def find_book!
    outcome = FindBook.run(params)
    return outcome.result if outcome.valid?
    not_found('exception')
  end
  
  # load only the needed ability file for a specific controller
  def current_ability
    @current_ability ||= BookAbility.new(current_user) 
  end

  # Whitelist parameters which are required
  #
  # @return [ActionController::Parameters]
  def book_params
    params.require(:book).permit(:title, :release_date, :user_id)
  end
end