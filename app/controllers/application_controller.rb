class ApplicationController < ActionController::Base
  protect_from_forgery
  
  def not_found(exception)
    render file: "#{Rails.root}/public/404.html", layout: false, status: 404
  end

  def after_sign_in_path_for(resource)
    return books_path if resource.is_a?(User)
    admin_root_path
  end

  rescue_from CanCan::AccessDenied do |exception|
    not_found(exception)
  end
end
