class CreateNewsHeadlines < ActiveRecord::Migration[6.1]
  def change
    create_table :news_headlines do |t|
      t.string :headline
      t.string :link
    end
  end
end
