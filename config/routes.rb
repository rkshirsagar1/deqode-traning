Rails.application.routes.draw do
  if Rails.env.development?
    mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/graphql"
  end
  post "/graphql", to: "graphql#execute"
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations',
    passwords: 'users/passwords',
    confirmations: 'users/confirmations'
  }
  require 'sidekiq-scheduler/web'
  require 'sidekiq/web'
  mount Sidekiq::Web => "/sidekiq"
  get '/daily_news', to: 'home#daily_news'
  root 'home#index'
  resources :books
end
